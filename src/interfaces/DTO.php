<?php

namespace blakit\api\interfaces;

interface DTO
{
    public function toJSON();
}